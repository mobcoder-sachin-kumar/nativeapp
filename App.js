import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  NativeModules,
} from 'react-native';
import React from 'react';

const App = () => {
  // const calendarModule = NativeModules.CalendarModule;
  const {CalendarModuleFoo} = NativeModules;

  const onPressAndroid = async () => {
    // simple
    // calendarModule.showMessage('Hello i am native toast of android', 1000);
    // calendarModule.createCalendarEvent('Party', 'My House');
    //callBack
    // calendarModule.doCallbackTask(
    //   11,
    //   (name, email, age) => {
    //     calendarModule.showMessage(
    //       `Result : name = ${name}, email = ${email}, age = ${age}`,
    //       1000,
    //     );
    //   },
    //   errorMessage => {
    //     calendarModule.showMessage(`Error : ${errorMessage}, 1000`);
    //   },
    // );
    //promise;
    // try {
    //   let result = await calendarModule.doPromiseTask(100);
    //   calendarModule.showMessage(`Result => ${JSON.stringify(result)}`, 1000);
    // } catch (error) {
    //   calendarModule.showMessage(`Error => ${error}`, 1000);
    // }
  };

  const onPressIos = async () => {
    // CalendarModuleFoo.createCalendarEvent('testName', 'testLocation');

    // CalendarModuleFoo.createCalendarEvent('Party', '04-12-2020', eventId => {
    //   console.log(`Created a new event with id ${eventId}`);
    // });

    // CalendarModuleFoo.createCalendarEventCallBack(
    //   'Party',
    //   '04-12-2020',
    //   (error, eventId) => {
    //     if (error) {
    //       console.error(`Error found! ${error}`);
    //     }
    //     console.log(`event id ${eventId} returned`);
    //   },
    // );

    // CalendarModuleFoo.createCalendarEventCallback(
    //   'testName',
    //   'testLocation',
    //   error => {
    //     console.error(`Error found! ${error}`);
    //   },
    //   eventId => {
    //     console.log(`event id ${eventId} returned`);
    //   },
    // );

    //promises
    try {
      const eventId = await CalendarModuleFoo.createCalendarEvent(
        'Party',
        'my house',
      );
      console.log(`Created a new event with id ${eventId}`);
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <SafeAreaView>
      <TouchableOpacity onPress={() => onPressIos()}>
        <Text>Click to invoke native modules!</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create({});
