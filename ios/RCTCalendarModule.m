//
//  RCTCalendarModule.m
//  nativeApp
//
//  Created by Mobcoder ID-227 on 07/02/23.
//

// RCTCalendarModule.m


#import "RCTCalendarModule.h"
#import <React/RCTLog.h>

@implementation RCTCalendarModule

// To export a module named RCTCalendarModule
RCT_EXPORT_MODULE(CalendarModuleFoo);

RCT_EXPORT_METHOD(createCalendarEvent:(NSString *)name location:(NSString *)location)
{
  RCTLogInfo(@"Pretending to create an event %@ at %@", name, location);
}

//callback
// RCT_EXPORT_METHOD(createCalendarEvent:(NSString *)title location:(NSString *)location myCallback:(RCTResponseSenderBlock)callback){
//                     NSInteger eventId = 11;
//                     RCTLogInfo(@"Pretending to create an event %@ at %@", title, location);
//                     callback(@[@(eventId)]);
  
// }

// callback with error handling
// RCT_EXPORT_METHOD(createCalendarEventCallBack:(NSString *)title location:(NSString *)location myCallback:(RCTResponseSenderBlock) callback){
//                     NSNumber *eventId = [NSNumber numberWithInt:123];
//                     RCTLogInfo(@"Pretending to create an event %@ at %@", title, location);
//                     callback(@[[NSNull null], eventId]);
  
// }


// with SuccessCallback & failureCallBack
// RCT_EXPORT_METHOD(createCalendarEventCallback:(NSString *)title location:(NSString *)location errorCallback: (RCTResponseSenderBlock)errorCallback successCallback: (RCTResponseSenderBlock)successCallback)
//     {
//         @try {
//             NSNumber *eventId = [NSNumber numberWithInt:123];
//             successCallback(@[eventId]);
//         }

//         @catch ( NSException *e ) {
//             errorCallback(@[e]);
//         }
//     }

//Promises
RCT_EXPORT_METHOD(createCalendarEvent:(NSString *)title
                 location:(NSString *)location
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
 NSInteger eventId = createCalendarEvent();
 if (eventId) {
    resolve(@(eventId));
  } else {
    reject(@"event_failure", @"no event id returned", nil);
  }
}

@end
