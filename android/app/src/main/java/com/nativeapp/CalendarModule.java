package com.nativeapp; // replace com.your-app-name with your app’s name
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;
import android.util.Log;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.WritableMap;

public class CalendarModule extends ReactContextBaseJavaModule {
   CalendarModule(ReactApplicationContext context) {
       super(context);
   }

   @Override
   public String getName(){
    return "CalendarModule";
   }

   @ReactMethod
    public void createCalendarEvent(String name, String location){
        Log.d("CalendarModule", "Create event called with name: " + name
                + " and location: " + location);
    }

    @ReactMethod
    public void showMessage(String message, int duration){
        Log.d("Toast message", message);
        android.widget.Toast.makeText(getReactApplicationContext(), message, duration).show();
    }

    @ReactMethod
    public void doCallbackTask(int aNumber, Callback successCallback, Callback failureCallback ){
            try {
                    if(aNumber == 100){
                        throw new Exception("Input number is 100, cannot do this task!");
                    }
                    String name = "Sachin";
                    String email = "sachin.kumar@mobcoder.com";
                    int age = 24;
                    successCallback.invoke(name, email, age);
            }
            catch (Exception e){
                    failureCallback.invoke(e.getMessage());
            }
    }

    @ReactMethod
    public void doPromiseTask(int aNumber, Promise promise) {
        try {
            if (aNumber == 100){
                throw new Exception("I hate 100!");
            }
            WritableMap mapResult = Arguments.createMap();
            mapResult.putString("name", "sachin");
            mapResult.putString("email", "sachin.kumar@mobcoder.com");
            mapResult.putInt("age", 24);
            promise.resolve(mapResult);
        } catch(Exception e) {
            promise.reject("An error occured", e);
        }
    }
   
}